let images = document.querySelectorAll('.image-to-show');
let timerID= setTimeout(play, 10000);
let active = 0;

function play(){
   images[active].classList.remove('open');
   if(active === images.length-1){
        active = 0;
   }else{
        active++;
   }
   images[active].classList.add('open');
   timerID = setTimeout(play, 10000);
}

document.querySelector('.stop').onclick = function(){
    clearTimeout(timerID);
};
document.querySelector('.run').onclick = function(){
    setTimeout(play, 10000);
};

